# webfonts

Web fonts optimized for Chrome to grab SVGs, not WOFFs

## Usage

Some fonts are commercial, avoid using them on production server

## Default Push URL

```
https://gitlab.com/englishextra/webfonts.git
```

## Remotes

* [GitHub](https://github.com/englishextra/webfonts)
* [BitBucket](https://bitbucket.org/englishextra/webfonts)
* [GitLab](https://gitlab.com/englishextra/webfonts)
